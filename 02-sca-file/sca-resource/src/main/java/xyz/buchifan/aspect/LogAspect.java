package xyz.buchifan.aspect;


import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class LogAspect {

    @Pointcut("@annotation(xyz.buchifan.annotation.RequiredLog)")
    public void dolog(){}

    @Around("dolog()")
    public Object doaround(ProceedingJoinPoint joinPoint) throws Throwable {
        log.debug("Before{}",System.currentTimeMillis());
        Object result = joinPoint.proceed();
        log.debug("after{}",System.currentTimeMillis());
        return result;
    }

}
