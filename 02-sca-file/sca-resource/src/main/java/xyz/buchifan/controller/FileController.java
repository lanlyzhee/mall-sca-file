package xyz.buchifan.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import xyz.buchifan.annotation.RequiredLog;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/file/")
public class FileController {

    @Value("${jt.resource.path}")
    private File resourcePath;

    @Value("${jt.resource.host}")
    private String resourceHost;

    @RequiredLog
    @GetMapping("/getTest")
    public String getTestAop() {
        System.out.println("测试成功");
        return "测试!!";
    }

    @PostMapping("/upload/")
    public String uploadFile(MultipartFile uploadFile) throws IOException{
        String path = DateTimeFormatter.ofPattern("yyyy/MM/dd")
                .format(LocalDate.now());
        File folder = new File(resourcePath, path);
        folder.mkdirs();
        String fileName = uploadFile.getOriginalFilename();
        String ext = fileName.substring(fileName.lastIndexOf("."));
        String name = UUID.randomUUID().toString()+ext;
        File file = new File(folder,name);

        uploadFile.transferTo(file);
        String url = resourceHost+"/"+path+"/"+name;
        log.debug("访问路径:"+url);
        return url;
    }
}