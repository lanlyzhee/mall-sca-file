package xyz.buchifan.config;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * 构建配置安全对象
 * 1)认证规则(哪些资源必须认证才可访问)
 * 2)加密规则(添加用户时密码写到了数据库,登录时要将输入的密码与数据查询出的密码进行比对)
 * 3)认证成功怎么处理?(跳转页面,返回json)
 * 4)认证失败怎么处理?(跳转页面,返回json)
 * 5)没有登录就去访问资源系统怎么处理?(返回登录页面,返回json)
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 密码加密
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    @Bean
    public AuthenticationManager authenticationManagerBean()
            throws Exception {
        return super.authenticationManagerBean();
    }
    ;

    /**
     * 定义认证规则
     *
     * @param http 安全对象
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();
        http.formLogin() // 表单登录
                // http.httpBasic() // HTTP Basic
                .loginPage("/login.html")
                .loginProcessingUrl("/login")
                .and()
                .authorizeRequests() // 授权配置
                .antMatchers("/login.html").permitAll()
                .anyRequest()  // 所有请求
                .authenticated(); // 都需要认证
        //登录成功与失败的处理
//        http.formLogin()
//                .loginPage("/login.html")
//                .loginProcessingUrl("/defaulting")
////                .usernameParameter("username")
////                .passwordParameter("password")
//                .successHandler(successHandler())
//                .failureHandler(failureHandler())
//                .defaultSuccessUrl("/index.html");
        //放行所有请求
//        http.authorizeRequests().anyRequest().permitAll();
//        http.authorizeRequests()
//                //设置要放行的咨询
//                .antMatchers("/login.html").permitAll()
//                //设置需要认证的请求（除了上面的要放行，其它都要进行认证）
//                .anyRequest().authenticated();
    }

    /**
     * 登录成功以后的处理器
     */
    /*
       @bean 可以交给spring处理
     */
    private AuthenticationSuccessHandler successHandler() {
        //当接口中只有一个抽象方法时,构建接口的实现类对象,可以参考如下写法:
        return (request, response, authentication) -> {
            Map<String, Object> map = new HashMap<>();
            map.put("state", 200);
            map.put("message", "login ok");
            writeJsonToClient(response, map);
        };
    }

    private void writeJsonToClient(HttpServletResponse response,
                                   Object object) throws IOException {
        //1.将对象转换为json
        //将对象转换为json有3种方案:
        //1)Google的Gson-->toJson  (需要自己找依赖)
        //2)阿里的fastjson-->JSON (spring-cloud-starter-alibaba-sentinel)
        //3)Springboot web自带的jackson-->writeValueAsString (spring-boot-starter-web)
        //我们这里借助springboot工程中自带的jackson
        //jackson中有一个对象类型为ObjectMapper,它内部提供了将对象转换为json的方法
        //例如:
        String jsonStr = new ObjectMapper().writeValueAsString(object);
        //3.将json字符串写到客户端
        PrintWriter writer = response.getWriter();
        writer.println(jsonStr);
        writer.flush();
    }

    /**
     * 登录失败的处理器
     */
    private AuthenticationFailureHandler failureHandler() {
        return (request, response, exception) -> {
            Map<String, Object> map = new HashMap<>();
            map.put("state", 500);
            map.put("message", "username or password error");
            writeJsonToClient(response, map);
        };
    }

    /**
     * 假如没有登录访问资源时给出提示
     */
    private AuthenticationEntryPoint entryPoint() {
        return (request, response, exception) -> {
            Map<String, Object> map = new HashMap<>();
            map.put("state", 500);
            map.put("message", "please login");
            //思考除了返回这些信息,还要返回什么?(JWT令牌)
            writeJsonToClient(response, map);

        };
    }

}
