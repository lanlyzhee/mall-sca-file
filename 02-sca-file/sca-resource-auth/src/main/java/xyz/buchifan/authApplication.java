package xyz.buchifan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class authApplication {
    public static void main(String[] args) {
        SpringApplication.run(
                authApplication.class,
                args);
//        encodePwd();
    }
//    static void encodePwd(){
//        BCryptPasswordEncoder encoder=new BCryptPasswordEncoder();
//        String password="123456";//明文
//        String newPwd=encoder.encode("123456");
//        System.out.println(newPwd);
//        //$2a$10$/pq85c2uWv5DPcD/onFwPO0jUXhleUvy.ocK/x0f5rfjFfVRm90Rm
//    }
//————————————————
//    版权声明：本文为CSDN博主「雨田说码」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
//    原文链接：https://blog.csdn.net/maitian_2008/article/details/118896800
}