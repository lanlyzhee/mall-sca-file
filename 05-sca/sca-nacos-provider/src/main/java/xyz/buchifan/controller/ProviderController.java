package xyz.buchifan.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import xyz.buchifan.ScaProviderApplication;

import java.util.concurrent.ThreadPoolExecutor;


@RestController
@RefreshScope
public class ProviderController {

    private static final Logger log =
            LoggerFactory.getLogger(ScaProviderApplication.class);
    @Value("${server.port}:8080")
    private String server;

    //http://localhost:8090/provider/echo
    @GetMapping(value = "/provider/echo/{msg}")
    public String doEcho(@PathVariable String msg) throws InterruptedException {
//        Thread.sleep(100);
        log.debug("====debug===");
        return server + " say:Hello Nacos Discovery" + msg;
    }

    @Value("${logging.level.xyz.buchifan:error}")
    private String Loglevel;

    @Value("${usr.age}")
    Integer age;
    //http://localhost:8090/provider/getLog
    @GetMapping(value = "/provider/getLog")
    public String getLog() throws InterruptedException {
//        Thread.sleep(100);

        age++;
        log.trace("====trace====");
        log.debug("====debug===");
        log.info("===info===");
        log.warn("===warn===");
        log.error("===error===");
        System.out.println("========================");
        return server + "Loglevel" + Loglevel+"age:"+age;
    }
}
