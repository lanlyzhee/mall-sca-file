package xyz.buchifan;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//启动类
@SpringBootApplication
public class ScaProviderApplication {
    public static void main(String[] args){
        SpringApplication.run(ScaProviderApplication.class,args);
    }
}

