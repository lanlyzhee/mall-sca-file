package xyz.buchifan.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import xyz.buchifan.factory.ProviderFallbackFactory;

@FeignClient(name = "sca-provider",
        contextId = "remoteProviderService",
        fallbackFactory = ProviderFallbackFactory.class)
public interface RemoteProviderService {
    @GetMapping("/provider/echo/{string}")
    public String echoMessage(@PathVariable("string") String string);
}
