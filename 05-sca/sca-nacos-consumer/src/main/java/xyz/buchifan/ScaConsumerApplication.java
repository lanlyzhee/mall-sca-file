package xyz.buchifan;


import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import xyz.buchifan.feign.RemoteProviderService;

import java.util.concurrent.atomic.AtomicLong;

@EnableFeignClients
@SpringBootApplication
public class ScaConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScaConsumerApplication.class, args);
    }


    @Autowired
    private RestTemplate loadBalancedRestTemplate;

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @RestController
    public class ConsumerController {

        @Autowired
        private RemoteProviderService remoteProviderService;

        @Value("${spring.application.name}")
        private String appName;

        @Autowired
        private RestTemplate restTemplate;
        //@LoadBalanced 负载均衡
        @Autowired
        private LoadBalancerClient loadBalancerClient;

        @GetMapping("/consumer/ehco/{msg}")
        //localhost:8083/consumer/ehco/消息
        public String doFeignEcho(@PathVariable String msg){
            return remoteProviderService.echoMessage(msg);
        }

        private AtomicLong atomicLong = new AtomicLong(1);//多线程共享,线程安全
//        int count;
        @GetMapping("/consumer/doRestEcho1")
        public String doRestEcho01() throws InterruptedException {
            long num = atomicLong.getAndIncrement();
            if(num%2==0){
                Thread.sleep(1000);
            }
//            String url = "http://localhost:8091/provider/echo/" + appName;
//            System.out.println("request url:" + url);
//            return restTemplate.getForObject(url, String.class);

            String url = String.format("http://%s/provider/echo/%s", "sca-provider", appName);
            //向服务提供方发起http请求,获取响应数据
            return loadBalancedRestTemplate.getForObject(
                    url,//要请求的服务的地址
                    String.class);//String.class为请求服务的响应结果类型
        }


        @GetMapping("/consumer/doRestEcho02")
        public String doRestEcho02() {
            ServiceInstance serviceInstance = loadBalancerClient.choose("sca-provider");
            String url = String.format("http://%s:%s/provider/echo/%s", serviceInstance.getHost(), serviceInstance.getPort(), appName);
            System.out.println("request url:" + url);
            return restTemplate.getForObject(url, String.class);
        }


        @GetMapping("/consumer/doRestEcho3")
        public String doRestEcho03() {
            String url = String.format("http://%s/provider/echo/%s", "sca-provider", appName);
            //向服务提供方发起http请求,获取响应数据
            return loadBalancedRestTemplate.getForObject(
                    url,//要请求的服务的地址
                    String.class);//String.class为请求服务的响应结果类型
        }

        @GetMapping("/consumer/findById")
        @SentinelResource("res")
        public String doFindById(@RequestParam("id") Integer id){
            return "resource id is"+id;
        }

    }

}
