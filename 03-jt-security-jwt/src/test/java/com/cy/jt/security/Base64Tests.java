package com.cy.jt.security;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Base64;

@SpringBootTest
public class Base64Tests {
    @Test
    void testEncodeDecode(){
        //1.定义目标字符串(对这个字符串进行编解码)
        String header="www.tedu.cn";
        //2.构建Base64编码对象Encoder
        Base64.Encoder encoder
                = Base64.getEncoder();//Encoder编码对象
        //3.对目标字符串进行编码
        String encodeStr =
                encoder.encodeToString(header.getBytes());
        System.out.println(encodeStr);//d3d3LnRlZHUuY24=
        //4.对已编码的对象进行解码
        //4.1获取一个解码对象(Decoder 解码对象)
        Base64.Decoder decoder = Base64.getDecoder();
        //4.2执行解码操作
        byte[] bytes=decoder.decode(encodeStr);
        //4.3构建字符串
        System.out.println(new String(bytes));
    }
}
