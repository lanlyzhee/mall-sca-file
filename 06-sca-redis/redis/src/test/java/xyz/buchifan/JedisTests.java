package xyz.buchifan;

import com.google.gson.Gson;
import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JedisTests {

    //字符串类型练习
    @Test
    public void testStringOpera() throws InterruptedException {
        //建立连接
//        Jedis jedis=new Jedis("192.168.126.130",6379);
        Jedis jedis = new Jedis("127.0.0.1", 6379);
//        jedis.auth("123456");//密码
        String result = jedis.ping();
        System.out.println(result);
        //存储数据
        jedis.set("count", "1");
        jedis.set("id", "10001");
        jedis.set("content", "aaafsdf");
        //更新数据
        jedis.expire("id", 1);//设置key有效时长
        jedis.incr("count");//key为count的自增
        //获取数据
        String count = jedis.get("count");
        Thread.sleep(1000);
        String id = jedis.get("id");
        Long num = jedis.strlen("content");
        System.out.println("cart.count=" + count);
        System.out.println("id=" + id);
        System.out.println("num=" + num);

        //释放资源
        jedis.close();
    }

    @Test
    public void testJsonOpera() {
        Map<String, Object> map = new HashMap<>();
        //构建对象
        map.put("id", 100);
        map.put("title", "spring认证");
        map.put("content", "very good");
        //对象转为json
        Gson gson = new Gson();
        String jsonStr = gson.toJson(map);
        //json写入redis
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        jedis.set("usr", jsonStr);
        //读取redis数据
        jsonStr = jedis.get("usr");
        System.out.println(jsonStr);
        Map<String, Object> obj = gson.fromJson(jsonStr, Map.class);
        System.out.println(obj);
        jedis.close();
    }

    //hash类型练习
    @Test
    public void testHashOpera_1() {
        //建立连接
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        //存储一篇博客信息
        jedis.hset("article", "id", "1");
        jedis.hset("article", "title", "mybatis");
        jedis.hset("article", "content", "framework ");
        //获取博客内容并输出
        String id = jedis.hget("article", "id");
        String title = jedis.hget("article", "title");
        String content = jedis.hget("article", "content");
        System.out.println(id + "/" + title + "/" + content);
        //释放资源
        jedis.close();
    }

    //hash类型练习 直接存储map对象
    @Test
    public void testHashOpera_2() {
        //1.建立连接
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        //2.存储一篇博客信息
        Map<String,String> map=new HashMap<>();
        map.put("x","100");
        map.put("y","200");
        jedis.hmset("point",map);//hset=>hmset 存取map https://tool.oschina.net/uploads/apidocs/redis/clients/jedis/Jedis.html
        //3.获取博客内容并输出
        map=jedis.hgetAll("point");
        System.out.println(map);
        //4.释放资源
        jedis.close();
    }
    //list实现秒杀队列
    @Test
    public void testListOpera_1(){
        //连接redis
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        //队列存数据
        List<String> list = jedis.brpop(4,"list1");
        System.out.println(list);
        jedis.brpop(4,"list1");
        jedis.brpop(4,"list1");
        jedis.brpop(4,"list1");
        //先进先出顺序队列存取数据
        //释放资源
        jedis.close();
    }

    //set类型
    @Test
    public void testSetOpera_1(){

        Jedis jedis = new Jedis("127.0.0.1", 6379);
        jedis.flushAll();
        jedis.sadd("count", "1","2","2");
        Set<String> set = jedis.smembers("count");
        System.out.println(set);
        jedis.close();
    }
}
