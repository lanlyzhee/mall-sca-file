package xyz.buchifan;

import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisPoolTests {
    @Test
    public void testJedisPool(){
        //连接池
        JedisPoolConfig config=new JedisPoolConfig();
        config.setMaxTotal(1000);//最大连接数
        config.setMaxIdle(60);//最大空闲数
        //创建连接池
        JedisPool jedisPool=
                new JedisPool(config,"127.0.0.1",6379);
        Jedis resource = jedisPool.getResource();
        resource.set("class","cgb2104");
        String clazz = resource.get("class");
        System.out.println(clazz);
        resource.close();
        jedisPool.close();
    }
}
