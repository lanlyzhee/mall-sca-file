package xyz.buchifan;

import redis.clients.jedis.Jedis;

/**
 * 业务需求:生成一个分布递增的id
 * 分布式环境不会采用数据库表中自带的自增策略-auto-increment
 * 夺标基于这个方法生成id作为主键id值
 */
public class IdGeneratorDemo1 {
    public static Long getId(){
        Jedis jedis = new Jedis("127.0.0.1",6379);
        Long id = jedis.incr("id");
        jedis.close();
        return id;
    }
    public static void main(String[] args) {
        for(int i=0;i<10;i++){
            new Thread(){
                @Override
                public void run() {
//                    super.run();
                    String tName = Thread.currentThread().getName();;
                    System.out.println(tName+"->"+
                            IdGeneratorDemo1.getId());
                }
            }.start();
        }
    }
}
