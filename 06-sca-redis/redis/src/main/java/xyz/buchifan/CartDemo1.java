package xyz.buchifan;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisDataException;

import java.util.Map;
import java.util.jar.JarEntry;

/**
 * 购物车:添加商品(用户id,产品id,产品数量),列出商品(用户id)
 */
public class CartDemo1 {
    public static void addCart(Long userId, Long productId, int num) {
        Jedis jedis = new Jedis("127.0.0.1",6379);
//        String product = jedis.hget("cart:"+userId, String.valueOf(productId));
        jedis.hincrBy("cart:"+userId, String.valueOf(productId), num);
        jedis.close();
    }
    public static Map<String,String> listCart(Long userId){
        Jedis jedis = new Jedis("127.0.0.1",6379);
        Map<String,String>map = jedis.hgetAll("cart:"+userId);
        jedis.close();
        return map;
    }
    public static void main(String[] args) {
        addCart(101L, 201L, 1);
        addCart(101L, 202L, 2);
        addCart(101L, 203L, 3);
        Map<String,String> map = listCart(101L);
        System.out.println(map);

    }
}
