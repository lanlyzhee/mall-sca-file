package xyz.buchifan;

import redis.clients.jedis.Jedis;
import sun.applet.resources.MsgAppletViewer;

/**
 * 秒杀队列
 * 业务逻辑:将商品抢购信息写到redis
 * 算法:队列FIFO
 */
public class SecondKillDemo1 {
//    private static Jedis jedis=
//            new Jedis("127.0.0.1",6379);
    //商品抢购-入队
    static void enque(String msg){
        Jedis jedis=new Jedis("127.0.0.1",6379);
        jedis.lpush("queue", msg);
        jedis.close();
    }
    //生成订单,扣减库存-底层异步出队
    static String deque(){
        Jedis jedis=new Jedis("127.0.0.1",6379);
        String res = jedis.rpop("queue");
        jedis.close();
        return res;
    }
    //测试-多次抢购,从队列获取内容
    public static void main(String[] args) {
        new Thread(){
            @Override
            public void run() {
                for (int i=1;i<=10;i++){
                    //模拟多个同时抢购
                    enque(String.valueOf(i));
                    try {
                        Thread.sleep(100);
                    }catch (Exception e){}
                }
//                super.run();
            }
        }.start();

        new Thread(){
            @Override
            public void run() {
                for(;;){
                    //后台处理订单
                    String msg = deque();
                    if(msg==null) continue;
                    System.out.println(msg);
                }
//                super.run();
            }
        }.start();
//        jedis.close();
    }
}
