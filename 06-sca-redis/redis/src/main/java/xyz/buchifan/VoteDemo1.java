package xyz.buchifan;

import redis.clients.jedis.Jedis;

import java.util.Set;
import java.util.jar.JarEntry;

/**
 * 投票模拟
 * 业务说明:一个用户只能投票一次
 * 设计:基于reids的set类型进行数据存储
 */
public class VoteDemo1 {
    private static Jedis jedis=
            new Jedis("127.0.0.1",6379);
//    static {
//        jedis.auth("123456");
//    }
    //进行投票
    static void vote(String activityId,String userId){
        jedis.sadd(activityId,userId);
    }
    //查看投票次数
    static Long getVoteCount(String activiryID){
        Long count = jedis.scard(activiryID);
        return count;
    }
    //查看获得被哪些用户投票
    static Set<String> getVoteUsers(String activityId){
        Set<String> smembers = jedis.smembers(activityId);
        return smembers;
    }
    //检查用户是否已经对活动投过票
    static Boolean checkVote(String activityId,String userId){
        Boolean flag = jedis.sismember(activityId, userId);
        return flag;
    }
    public static void main(String[] args) {
        //初始化
        String activityID = "10001";
        String user1 = "201";
        String user2 = "202";
        String user3 = "203";
        String user4 = "204";
        //投票
        vote(activityID, user1);
        vote(activityID, user2);
        vote(activityID, user3);
        //获取投票次数
        Long voteCount = getVoteCount(activityID);
        System.out.println("投票次数:"+voteCount);
        //输出哪些人投过票
        Set<String> users = getVoteUsers(activityID);
        System.out.println(users);
        //检查用户是否投过票
        boolean flag = checkVote(activityID, user1);
        System.out.println("user1:"+user1);
    }
}
