package xyz.buchifan;

import redis.clients.jedis.Jedis;

import java.util.*;

public class SSODemo01 {
    //校验session有效性
    public static boolean isValidSession(String token){
        //判断token
        if(token==null||"".equals(token)){
            return false;
        }
        //建立连接
        Jedis jedis = new Jedis("127.0.0.1",6379);
        //密码
//        jedis.auth("123456")
        //redis获取会话信息
        //根据token生成用户信息(json)
        String user = jedis.hget("session", token);
        System.out.println("user="+user);
        if(user==null||"".equals(user)){
            System.out.println("还没有登录");
            jedis.close();
            return false;
        }
        //判断是否登录超时
        String dateStr = jedis.hget("session", token+":expired");
        Date expired = new Date(Long.valueOf(dateStr));
        if(dateStr==null||"".equals(dateStr)||expired.before(new Date())){
            System.out.println("登录超时");
            jedis.close();
            return false;
        }
        return true;
    }
    //执行登录操作
    public static String login(String username,String password){
        System.out.println("基于"+username+"/"+password+"进行登录");
        //创建token
        String token = UUID.randomUUID().toString()
                .replace("-", "");
        System.out.println(token);
        //有效登录时长
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE,30);//30分钟
        //存储会话信息
        Map<String,String> map = new HashMap<>();
        map.put(token, username);//username为有用户信息的json串
        map.put(token+":expired",String.valueOf(calendar.getTimeInMillis()) );
        Jedis jedis = new Jedis("127.0.0.1",6379);
        jedis.hmset("session", map);
        jedis.close();
        return token;
    }
    //模拟登录
    public static void main(String[] args) {
        System.out.println("===访问系统资源===");
        //第一次访问:访问redis,检查是否有有效会话信息
        String token = null;
        boolean flag = isValidSession(token);//访问redis
        System.out.println(flag?"第一次:已登录":"第一次:未登录");
        //执行登录
        if(!flag){
            System.out.println("执行登录");
            token = login("jack", "123456");//用户输入的账号密码
            //成功的token响应到客户端
        }
        //第二次访问
        flag = isValidSession(token);
        System.out.println(flag?"第二次:已登录":"第二次:未登录");
        //第三次访问
    }
}
