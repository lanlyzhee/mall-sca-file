package xyz.buchifan.redis;

import com.sun.xml.internal.stream.StaxErrorReporter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@SpringBootTest
public class RedisTemplateTests {

    @Autowired
    private RedisTemplate redisTemplate;
    // 主要用于操作redis中的复杂对象

    @Test
    public void testConnection() {
        String result =
                redisTemplate.getConnectionFactory().getConnection().ping();
        //删除缓存
//        Set<Object> keys = redisTemplate.keys("*");
//        redisTemplate.delete(keys);
        System.out.println(result + ",并删除所有value");
    }

    //清空reids数据库数据
    @Test
    void testFlushAll(){
        redisTemplate.execute(new RedisCallback() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                connection.flushAll();
                return "flush ok";
            }
        });
    }

    @Test
    public void testSetData() {
        SetOperations setOperations = redisTemplate.opsForSet();
        setOperations.add("setkey1", "A", "B", "C", "C");
        Object members = setOperations.members("setkey1");
        System.out.println("setkey=" + members);
    }

    @Test
    void testListData() {
        //list集合放数据
        ListOperations listOperations = redisTemplate.opsForList();
        while (listOperations.rightPop("lstkey1") != null)
            listOperations.rightPop("lstkey1");
        listOperations.leftPush("lstkey1", "100");
        listOperations.leftPushAll("lstkey1", "200", "300");
        listOperations.leftPush("lstkey1", "100", "105");
        listOperations.rightPush("lstkey1", "700");
        Object value = listOperations.range("lstkey1", 0, -1);
        System.out.println(value);
        //list集合取数据
        Object v1 = listOperations.leftPop("lstkey1");
        System.out.println("left.pop.0=" + v1);
        value = listOperations.range("lstkey1", 0, -1);
        System.out.println(value);

    }
    //操作redis中的hash数据
    @Test
    void testHashData(){
        HashOperations hashOperations = redisTemplate.opsForHash();
        Map<String, String> blog = new HashMap<>();
        blog.put("id", "1");
        blog.put("title", "hello redis");
        hashOperations.putAll("blog",blog);
        hashOperations.put("blog", "content", "redis is good");
        Object hv = hashOperations.get("blog", "id");
        System.out.println(hv);
        Object entries= hashOperations.entries("blog");
        System.out.println("entries"+entries);

    }
}
