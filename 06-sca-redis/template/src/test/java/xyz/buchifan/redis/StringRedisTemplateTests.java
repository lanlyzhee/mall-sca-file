package xyz.buchifan.redis;


/**
 *  StringRedisTemplate是专门操作redis字符串类型数据的对象
 */

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.TimeoutUtils;
import org.springframework.data.redis.core.ValueOperations;

import java.util.concurrent.TimeUnit;

@SpringBootTest
public class StringRedisTemplateTests {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    public void testConnection(){
        RedisConnection connection =
                stringRedisTemplate.getConnectionFactory()
                .getConnection();
        String ping = connection.ping();
        System.out.println(ping);
    }

    @Test
    public void testRedisStringOpera() throws Exception{
        ValueOperations<String,String> valueOperations
                = stringRedisTemplate.opsForValue();
        valueOperations.set("ip", "127.0.0.1");
        valueOperations.set("state", "1",1, TimeUnit.SECONDS);
        valueOperations.decrement("state");

//        Thread.sleep(2000);
        String ip=valueOperations.get("ip");
        System.out.println("ip="+ip);
        String state=valueOperations.get("state");
        System.out.println("state="+state);
    }
}
