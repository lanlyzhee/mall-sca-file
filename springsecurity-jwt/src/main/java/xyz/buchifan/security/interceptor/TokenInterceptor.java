package xyz.buchifan.security.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import xyz.buchifan.security.utils.JwtUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TokenInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception {
        System.out.println("==prehandler==");
        String token = request.getHeader("Authentication");
        if(token==null||"".equals(token))
            throw new RuntimeException("please login");
        boolean flag = JwtUtils.isTokenExpired(token);
        if(flag)
            throw new RuntimeException("login timeout,plz login");
        return true;
    }
}
