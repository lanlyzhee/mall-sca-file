package xyz.buchifan.security.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceController {
    @RequestMapping("/retrieve")
    public String doRetrieve(){
        return "do retrieve resource success";
    }

    @RequestMapping("/update")
    public String doUpdate(){
        return "do update resource success";
    }
}
