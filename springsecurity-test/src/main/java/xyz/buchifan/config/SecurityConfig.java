package xyz.buchifan.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import xyz.buchifan.config.handler.DefaultAccessDeniedExceptionHandler;
import xyz.buchifan.config.handler.DefaultAuthenticationEntryPoint;
import xyz.buchifan.config.handler.DefaultAuthenticationSuccessHandler;
import xyz.buchifan.config.handler.RedirectAuthenticationSuccessHandler;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Bean //对象名默认为方法名
    //@Bean("bcryptPasswordEncoder")//bean对象名字为bcryptPasswordEncoder
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //super.configure(http);
        //关闭跨域攻击，不关闭容易出错
        http.csrf().disable();
        //自定义登陆表单
        http.formLogin()
                //设置登陆页面
                .loginPage("/login.html")
                //设置登陆请求处理地址(对应form表单中的action),登陆时会访问UserDetailService对象
                .loginProcessingUrl("/login")
                //设置请求用户名参数为username(默认就是username，可以自己修改，需要与表单同步)
                .usernameParameter("username")
                //请求请求密码参数为password(默认就是password，可以自己修改，需要与表单同步)
                .passwordParameter("password")
                //返回成功的json串
                .successHandler(new DefaultAuthenticationSuccessHandler());
                //重定向
                //.successHandler(new RedirectAuthenticationSuccessHandler("http://www.baidu.com"));
                //设置登陆成功跳转页面(默认为/index.html)
//                .defaultSuccessUrl("/index.html");
                //登陆失败访问的页面（默认为/login.html?error）
//                .failureUrl("/login.html?error");
        //认证设计
        http.authorizeRequests()
                //设置要放行的咨询
                .antMatchers("/login.html").permitAll()
                //设置需要认证的请求（除了上面的要放行，其它都要进行认证）
                .anyRequest().authenticated();
        http.exceptionHandling()
                .authenticationEntryPoint(new DefaultAuthenticationEntryPoint())
                .accessDeniedHandler(new DefaultAccessDeniedExceptionHandler());
    }
//————————————————
//    版权声明：本文为CSDN博主「雨田说码」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
//    原文链接：https://blog.csdn.net/maitian_2008/article/details/118896800
}
