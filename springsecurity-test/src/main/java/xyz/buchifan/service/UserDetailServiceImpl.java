package xyz.buchifan.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username)
        throws UsernameNotFoundException {
        System.out.println("加载输入user");
        if(!"user".equals(username)) {
            System.out.println("user not exists");
            throw new UsernameNotFoundException("user not exists");
        }
        String encodePwd = passwordEncoder.encode("123456");
        System.out.println(encodePwd);
        List<GrantedAuthority> grantedAuthorities=
                AuthorityUtils.commaSeparatedStringToAuthorityList(
                        "ROLE_admin,ROLE_normal,sys:res:retrieve,sys:res:create");
        User user = new User(username, encodePwd, grantedAuthorities);
        return user;
    }
}
