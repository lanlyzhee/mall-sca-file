package sso.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import sso.dao.UserMapper;

import java.util.List;
import java.util.Map;

/**
 * 通过此对象处理登录请求
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UserMapper userMapper;
    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        //1.基于用户名从数据库查询用户信息
        //SysUser user=userMapper.selectUserByUsername(username);//查数据库
        if(!"jack".equals(username))//假设这是从数据库查询的信息
            throw new UsernameNotFoundException("user not exists");
        //2.将用户信息封装到UserDetails对象中并返回
        //假设这个密码是从数据库查询出来的
        String encodedPwd=passwordEncoder.encode("123456");
        //假设这个权限信息也是从数据库查询到的
        //List<String> permissions=userMapper.selectUserPermissions(username);//查数据库
        //假如分配权限的方式是角色,编写字符串时用"ROLE_"做前缀
        List<GrantedAuthority> grantedAuthorities =
                AuthorityUtils.commaSeparatedStringToAuthorityList( "sys:res:retrieve,sys:res:create");
        //这个user是SpringSecurity提供的UserDetails接口的实现,用于封装用户信息
        //后续我们也可以基于需要自己构建UserDetails接口的实现
        User user=new User(username,encodedPwd,grantedAuthorities);
        return user;//这里的返回值会交给springsecurity去校验
//————————————————
//        版权声明：本文为CSDN博主「雨田说码」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
//        原文链接：https://blog.csdn.net/maitian_2008/article/details/118997430
//sdf
//

//        //1.基于用户名查询用户信息
//        Map<String,Object> userMap=userMapper.selectUserByUsername(username);
//        if(userMap==null)throw new UsernameNotFoundException("user not exists");
//        //2.查询用户权限信息并封装查询结果
//        List<String> userPermissions=
//        userMapper.selectUserPermissions((Long)userMap.get("id"));
//        //String password=passwordEncoder.encode("123456");
//        //System.out.println("password="+password);
////        List<GrantedAuthority> grantedAuthorities =
////                AuthorityUtils.commaSeparatedStringToAuthorityList(
////                        "sys:res:retrieve,sys:res:create");
//        //权限信息后续要从数据库去查
//        return new User(username,
//                (String)userMap.get("password"),//来自数据库
//                AuthorityUtils.createAuthorityList(//来自数据库
//                        userPermissions.toArray(new String[]{})));
//        //这个值返回给谁?谁调用此方法这个就返回给谁.
    }
}
