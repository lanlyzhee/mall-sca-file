package sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

//启动方法上的权限控制
@EnableGlobalMethodSecurity(prePostEnabled = true)
@SpringBootApplication
public class ResApp {

    public static void main(String[] args) {
        SpringApplication.run(ResApp.class,args);
    }
}
